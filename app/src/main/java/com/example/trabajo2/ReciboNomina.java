package com.example.trabajo2;
public class ReciboNomina {
    private int numRecibo;
    private String nombre;
    private int horasTrabajadasNormales;
    private int horasTrabajadasExtras;
    private int puesto;
    private double porcentajeImpuesto;

    public ReciboNomina(int numRecibo, String nombre, int horasTrabajadasNormales, int horasTrabajadasExtras, int puesto) {
        this.numRecibo = numRecibo;
        this.nombre = nombre;
        this.horasTrabajadasNormales = horasTrabajadasNormales;
        this.horasTrabajadasExtras = horasTrabajadasExtras;
        this.puesto = puesto;
        this.porcentajeImpuesto = 0.16; // 16%
    }

    private double calcularPagoPorHora() {
        double pagoBase = 200;
        switch (puesto) {
            case 1:
                return pagoBase + (pagoBase * 0.20);
            case 2:
                return pagoBase + (pagoBase * 0.50);
            case 3:
                return pagoBase + (pagoBase * 1.00);
            default:
                return pagoBase;
        }
    }

    public double calcularSubtotal() {
        double pagoPorHora = calcularPagoPorHora();
        return (horasTrabajadasNormales * pagoPorHora) + (horasTrabajadasExtras * pagoPorHora * 2);
    }

    public double calcularImpuesto() {
        return calcularSubtotal() * porcentajeImpuesto;
    }

    public double calcularTotalPagar() {
        return calcularSubtotal() - calcularImpuesto();
    }
}
