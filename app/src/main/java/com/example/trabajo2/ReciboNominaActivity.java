package com.example.trabajo2;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;

public class ReciboNominaActivity extends AppCompatActivity {

    private EditText numRecibo, nombre, horasTrabajadasNormales, horasTrabajadasExtras;
    private RadioGroup puestoGroup;
    private TextView subtotal, impuesto, total, nombreTrabajador;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recibo);

        nombreTrabajador = findViewById(R.id.nombreTrabajador);

        // Recibir el nombre pasado desde MainActivity
        String nombreRecibido = getIntent().getStringExtra("nombre");
        if (nombreRecibido != null) {
            nombreTrabajador.setText(nombreRecibido);
        }

        numRecibo = findViewById(R.id.numRecibo);
        nombre = findViewById(R.id.nombre);
        horasTrabajadasNormales = findViewById(R.id.horasTrabajadasNormales);
        horasTrabajadasExtras = findViewById(R.id.horasTrabajadasExtras);
        puestoGroup = findViewById(R.id.puestoGroup);
        subtotal = findViewById(R.id.subtotal);
        impuesto = findViewById(R.id.impuesto);
        total = findViewById(R.id.total);

        Button calcular = findViewById(R.id.calcular);
        calcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                calcularRecibo();
            }
        });

        Button limpiar = findViewById(R.id.limpiar);
        limpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                limpiarCampos();
            }
        });

        Button regresar = findViewById(R.id.regresar);
        regresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void calcularRecibo() {
        int numReciboInt = Integer.parseInt(numRecibo.getText().toString());
        String nombreStr = nombre.getText().toString();
        int horasNormales = Integer.parseInt(horasTrabajadasNormales.getText().toString());
        int horasExtras = Integer.parseInt(horasTrabajadasExtras.getText().toString());
        int puestoInt = getPuestoSeleccionado();

        ReciboNomina recibo = new ReciboNomina(numReciboInt, nombreStr, horasNormales, horasExtras, puestoInt);

        double subtotalValue = recibo.calcularSubtotal();
        double impuestoValue = recibo.calcularImpuesto();
        double totalPagarValue = recibo.calcularTotalPagar();

        subtotal.setText(String.valueOf(subtotalValue));
        impuesto.setText(String.valueOf(impuestoValue));
        total.setText(String.valueOf(totalPagarValue));
    }

    private int getPuestoSeleccionado() {
        int selectedId = puestoGroup.getCheckedRadioButtonId();
        if (selectedId == R.id.puestoAuxiliar) {
            return 1;
        } else if (selectedId == R.id.puestoAlbanil) {
            return 2;
        } else if (selectedId == R.id.puestoIngObra) {
            return 3;
        }
        return 1; // Default
    }

    private void limpiarCampos() {
        numRecibo.setText("");
        nombre.setText("");
        horasTrabajadasNormales.setText("");
        horasTrabajadasExtras.setText("");
        puestoGroup.clearCheck();
        subtotal.setText("");
        impuesto.setText("");
        total.setText("");
    }
}